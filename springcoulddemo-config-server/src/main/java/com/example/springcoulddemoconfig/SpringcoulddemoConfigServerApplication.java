package com.example.springcoulddemoconfig;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;

@SpringBootApplication
@EnableConfigServer
public class SpringcoulddemoConfigServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringcoulddemoConfigServerApplication.class, args);
	}

}
