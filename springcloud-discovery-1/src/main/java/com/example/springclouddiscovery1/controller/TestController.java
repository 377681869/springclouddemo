package com.example.springclouddiscovery1.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

/**
 * @Author:yangzibo
 * @CreateTime:2024-04-10 11:58
 * @Description:
 * @Version:1.0
 **/
@RestController
public class TestController {

    @GetMapping("/index")
    public String index(){
        return "test1";
    }


    private final RestTemplate restTemplate;

    @Autowired
    public TestController(RestTemplate restTemplate) {this.restTemplate = restTemplate;}

    @RequestMapping(value = "/echo/{str}", method = RequestMethod.GET)
    public String echo(@PathVariable String str) {
        return restTemplate.getForObject("http://ervice-provider/echo/" + str, String.class);
    }
}
