package com.example.springclouddiscovery1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class SpringcloudDiscovery1Application {

	public static void main(String[] args) {
		SpringApplication.run(SpringcloudDiscovery1Application.class, args);
	}

}
