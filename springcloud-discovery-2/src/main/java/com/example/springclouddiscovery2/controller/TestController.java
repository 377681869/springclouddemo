package com.example.springclouddiscovery2.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

/**
 * @Author:yangzibo
 * @CreateTime:2024-04-10 11:58
 * @Description:
 * @Version:1.0
 **/
@RestController
public class TestController {

    @RequestMapping(value = "/echo/{string}", method = RequestMethod.GET)
    public String echo(@PathVariable String string) {
        return "Hello Nacos Discovery " + string;
    }
}
