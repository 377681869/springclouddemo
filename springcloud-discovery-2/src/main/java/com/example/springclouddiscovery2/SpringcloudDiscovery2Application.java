package com.example.springclouddiscovery2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class SpringcloudDiscovery2Application {

	public static void main(String[] args) {
		SpringApplication.run(SpringcloudDiscovery2Application.class, args);
	}

}
