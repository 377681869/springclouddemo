package com.example.springcoulddemoconfig;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringcoulddemoConfigClientApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringcoulddemoConfigClientApplication.class, args);
	}
}
