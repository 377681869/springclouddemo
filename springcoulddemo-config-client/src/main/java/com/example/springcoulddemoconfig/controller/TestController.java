package com.example.springcoulddemoconfig.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author:yangzibo
 * @CreateTime:2024-04-08 21:57
 * @Description:
 * @Version:1.0
 **/
@RestController
@RequestMapping("test")
@RefreshScope
public class TestController {

    @Value("${ddzj.name}")
    private String ddzjName;

    @Autowired
    private Environment environment;

    @GetMapping("/index")
    public String index(){
       String s = environment.getProperty("ddzj.name");
        return "success" + s + this.ddzjName;
    }
}
