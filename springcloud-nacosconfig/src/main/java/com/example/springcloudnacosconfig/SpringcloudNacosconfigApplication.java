package com.example.springcloudnacosconfig;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringcloudNacosconfigApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringcloudNacosconfigApplication.class, args);
	}

}
