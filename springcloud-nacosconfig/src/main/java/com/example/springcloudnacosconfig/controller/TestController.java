package com.example.springcloudnacosconfig.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author:yangzibo
 * @CreateTime:2024-04-10 09:35
 * @Description:
 * @Version:1.0
 **/
@RestController
@RefreshScope
@RequestMapping("/test")
public class TestController {

    @Autowired
    private Environment environment;

    @RequestMapping("/index")
    public String index(){
        String name = environment.getProperty("ddzj.name");
        return "index" + name + environment.getProperty("ddzj.id");
    }



}
